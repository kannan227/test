import 'package:flutter/material.dart';

class Semester_three extends StatefulWidget {
  @override
  _Semester_threeState createState() => _Semester_threeState();
}

class _Semester_threeState extends State<Semester_three> {
  String mobile = 'S';
  String grapics = 'S';
  String tqm = 'S';
  String poc = 'S';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Semester Three'),
        backgroundColor: Colors.purple,
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.only(left: 60.0, right: 60.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Text(
                  'Mobile Computing',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 50.0),
                DropdownButton<String>(
                  value: mobile,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      mobile = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'Grapics Design',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 79.0),
                DropdownButton<String>(
                  value: grapics,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      grapics = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'TQM',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 171.0),
                DropdownButton<String>(
                  value: tqm,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      tqm = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'POC',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 173.0),
                DropdownButton<String>(
                  value: poc,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      poc = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            new RaisedButton(child: Text('Calculate'), onPressed: () {}),
          ],
        ),
      ),
    );
  }
}
