import 'package:cgpagpa/Semester%20five.dart';
import 'package:cgpagpa/Semester%20four.dart';
import 'package:cgpagpa/Semester%20one.dart';
import 'package:cgpagpa/Semester%20three%20.dart';
import 'package:flutter/material.dart';

import 'Semester two.dart';

class GPA extends StatefulWidget {
  @override
  _GPAState createState() => _GPAState();
}

class _GPAState extends State<GPA> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('GPA calculation'),
        backgroundColor: Colors.purple,
      ),
      body: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new RaisedButton(
                    child: Text(' Semester One '),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Semester_one()),
                      );
                    }),
                SizedBox(
                  height: 20.0,
                ),
                new RaisedButton(
                    child: Text(' Semester Two '),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Semester_two()),
                      );
                    }),
                SizedBox(
                  height: 20.0,
                ),
                new RaisedButton(
                    child: Text('Semester Three'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Semester_three()),
                      );
                    }),
                SizedBox(
                  height: 20.0,
                ),
                new RaisedButton(
                    child: Text(' Semester Four'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Semester_four()),
                      );
                    }),
                SizedBox(
                  height: 20.0,
                ),
                new RaisedButton(
                    child: Text(' Semester Five'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Semester_five()),
                      );
                    }),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
