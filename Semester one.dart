import 'package:flutter/material.dart';

class Semester_one extends StatefulWidget {
  Semester_one({Key key}) : super(key: key);
  @override
  _Semester_oneState createState() => _Semester_oneState();
}

class _Semester_oneState extends State<Semester_one> {
  String Maths = 'S';
  String c = 'S';
  String chemistry = 'S';
  String physics = 'S';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Semester One'),
        backgroundColor: Colors.purple,
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.only(left: 60.0, right: 60.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Text(
                  'Mathematics',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 100.0),
                DropdownButton<String>(
                  value: Maths,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      Maths = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'C++',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 180.0),
                DropdownButton<String>(
                  value: c,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      c = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'Physics',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 145.0),
                DropdownButton<String>(
                  value: physics,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      physics = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'Chemistry',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 125.0),
                DropdownButton<String>(
                  value: chemistry,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      chemistry = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            new RaisedButton(child: Text('Calculate'), onPressed: () {}),
          ],
        ),
      ),
    );
  }
}
