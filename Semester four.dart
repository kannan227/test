import 'package:flutter/material.dart';

class Semester_four extends StatefulWidget {
  @override
  _Semester_fourState createState() => _Semester_fourState();
}

class _Semester_fourState extends State<Semester_four> {
  String compile = 'S';
  String pqt = 'S';
  String computer = 'S';
  String operting = 'S';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Semester Four'),
      ),
      body: Container(
        margin: EdgeInsets.only(left: 60.0, right: 60.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Text(
                  'Compiler Design',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 50.0),
                DropdownButton<String>(
                  value: compile,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      compile = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'PQT',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 160.0),
                DropdownButton<String>(
                  value: pqt,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      pqt = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'Computer Networks',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 20.0),
                DropdownButton<String>(
                  value: computer,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      computer = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'Operating System',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 40.0),
                DropdownButton<String>(
                  value: operting,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      operting = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            new RaisedButton(child: Text('Calculate'), onPressed: () {}),
          ],
        ),
      ),
    );
  }
}
