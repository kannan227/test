import 'package:flutter/material.dart';

class CGPA extends StatefulWidget {
  CGPA({Key key}) : super(key: key);
  @override
  _CGPAState createState() => _CGPAState();
}

class _CGPAState extends State<CGPA> {
  String dropdownValue = 'S';
  String dropdownValue2 = 'S';
  String dropdownValue3 = 'S';
  String dropdownValue4 = 'S';
  String dropdownValue5 = 'S';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CGPA calculator'),
        centerTitle: true,
        backgroundColor: Colors.purple,
      ),
      body: Container(
        margin: EdgeInsets.only(left: 60.0, right: 60.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Text(
                  'Semester 1',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 100.0),
                DropdownButton<String>(
                  value: dropdownValue,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      dropdownValue = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'Semester 2',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 100.0),
                DropdownButton<String>(
                  value: dropdownValue2,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      dropdownValue2 = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'Semester 3',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 100.0),
                DropdownButton<String>(
                  value: dropdownValue3,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      dropdownValue3 = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'Semester 4',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 100.0),
                DropdownButton<String>(
                  value: dropdownValue4,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      dropdownValue4 = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'Semester 5',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 100.0),
                DropdownButton<String>(
                  value: dropdownValue5,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      dropdownValue5 = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            new RaisedButton(child: Text('Calculate'), onPressed: () {}),
          ],
        ),
      ),
    );
  }
}
