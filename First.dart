import 'package:cgpagpa/GPA.dart';
import 'package:flutter/material.dart';

import 'CGPA.dart';

class First extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('GPA&CGPA CALCULATOR'),
          centerTitle: true,
          backgroundColor: Colors.purple,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new RaisedButton(
                child: Text(
                  'GPA calculation',
                  style: TextStyle(
                      fontSize: 12.0,
                      fontStyle: FontStyle.normal,
                      color: Colors.purple),
                ),
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => GPA()));
                },
                color: Colors.white,
              ),
              new RaisedButton(
                child: Text(
                  'CGPA calculation',
                  style: TextStyle(
                      fontSize: 11.0,
                      fontStyle: FontStyle.normal,
                      color: Colors.purple),
                ),
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => CGPA()));
                },
                color: Colors.white,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
