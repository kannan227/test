import 'package:flutter/material.dart';

class Semester_two extends StatefulWidget {
  @override
  _Semester_twoState createState() => _Semester_twoState();
}

class _Semester_twoState extends State<Semester_two> {
  String distrubted = 'S';
  String java = 'S';
  String data = 'S';
  String system = 'S';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Semester Two'),
        centerTitle: true,
        backgroundColor: Colors.purple,
      ),
      body: Container(
        margin: EdgeInsets.only(left: 60.0, right: 60.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Text(
                  'Distrubted System',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 50.0),
                DropdownButton<String>(
                  value: distrubted,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      distrubted = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'Java',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 170.0),
                DropdownButton<String>(
                  value: java,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      java = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'Data Structure',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 85.0),
                DropdownButton<String>(
                  value: data,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      data = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            new Row(
              children: <Widget>[
                new Text(
                  'System Design',
                  style: TextStyle(color: Colors.purple, fontSize: 20.0),
                ),
                SizedBox(width: 80.0),
                DropdownButton<String>(
                  value: system,
                  style: TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      system = newValue;
                    });
                  },
                  items: <String>['S', 'A', 'B', 'C', 'D', 'E', 'U']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            new RaisedButton(child: Text('Calculate'), onPressed: () {}),
          ],
        ),
      ),
    );
  }
}
